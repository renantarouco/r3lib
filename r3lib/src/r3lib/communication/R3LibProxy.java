/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication;

import bftsmart.tom.ServiceProxy;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import r3lib.communication.proxy.ConsensusProxy;
import r3lib.communication.proxy.ProxyFactory;

/**
 *
 * @author paola
 */
public class R3LibProxy {

    private final ConsensusProxy delivery;

    public R3LibProxy(int paxosClientId) throws IOException, InterruptedException {
        // TODO ler arquivo de configuração para saber qual delivery instanciar

        FileInputStream fis = new FileInputStream("config.properties");
        Properties prop = new Properties();
        prop.load(fis);
        String protocol = prop.getProperty("agreement.protocol");
        System.out.println("agreement protocol: " + protocol);

        delivery = ProxyFactory.createProxy(protocol, paxosClientId);
    }

    public Response newLib(Path libPath, String libName) throws IOException {
        Response r = null;

        //File jarfile = new File("/home/paola/Documents/tcc/genericrsm/KeyValueService/dist/KeyValueService.jar"); 
        byte[] content = Files.readAllBytes(libPath);

        try {
            // registrando tipo
            Command cmd = new Command(libName, content);

            // enviando requisição
            r = delivery.send("", cmd);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro na execução do programa");
        }

        return r;
    }

    public Response newAppInstance(String libName, String instanceName) {
        Response r = null;

        try {
            // instanciando a lib
            Command cmd = new Command(libName, instanceName);

            // enviando requisição
            r = delivery.send("", cmd);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro na execução do programa");
        }
        return r;
    }

    public Response invokeMethod(String url, Object... params) {

        Response r = null;

        try {
            // registrando tipo
            Command cmd = new Command(url, params);

            // enviando requisição
            r = delivery.send(url.split("\\.")[0], cmd);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro na execução do programa");
        }
        return r;
    }

}
