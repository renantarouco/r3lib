/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication;

import java.io.Serializable;

/**
 *
 * @author paola
 */
public class Command implements Serializable {
    public String smrCmd;
    public String str0;
    public String str1;
    public Object[] params;
    public byte[] libSource;
    

    public Command(String smrCmd, String str0, String str1, byte[] libSource, Object[] params) {
        this.smrCmd = smrCmd;
        this.params = params;
        this.libSource = libSource;
        this.str0 = str0;
        this.str1 = str1;
    }

    public Command(String url, Object[] params){
        this.smrCmd = "invoke_method";
        this.str0 = url;
        this.params = params;
    }

    public Command(String str0, byte[] libSource){
        this.smrCmd = "register_type";
        this.libSource = libSource;
        this.str0 = str0;
    }
    
    public Command(String str0, String str1){
        this.smrCmd = "new_app_instance";
        this.str0 = str0;
        this.str1 = str1;
    }
    
    
    
}
