/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.KeyValueStore;
import model.Person;
import r3lib.annotations.SmrMethod;

/**
 *
 * @author paola
 */
public class KeyValueController {

    private final KeyValueStore peopleStore;

    public KeyValueController() {
        this.peopleStore = KeyValueStore.getInstance();
        //peopleStore = new KeyValueStore();
    }

    @SmrMethod(description = "Returns the result of the sum of two integers")
    public void add(int key, Person person) {
        this.peopleStore.addPerson(key, person);
    }

    @SmrMethod(description = "Returns the result of the sum of two integers")
    public void remove(int key) {
        this.peopleStore.removePerson(key);
    }

    @SmrMethod(description = "Returns the result of the sum of two integers")
    public Person get(int key) {
        return this.peopleStore.getPerson(key);
    }

    public void registerLog(String log) {
        System.out.println(log);
    }

}
